/*
Nama   	 : David Aditya Susanto
NPM      : 140810190067
Deskripsi: Reverse Queue
Tanggal  : 3 Mei 2020
*/
#include <iostream>
using namespace std;

struct element{
		
		char info;
		element *next;
};

struct queue{
		
		element *front;
		element *back;
};

queue q;
queue r;

void createQueue(queue &q){
		
		q.front = NULL;
		q.back = NULL;
}

void createElement(element *&new_element);
void createElement2(element *&new_element,char& elm);
bool isEmpty(queue q){
		
		if(q.front == NULL){
				return true;
		}else{
				return false;
		}
}

void enqueue(queue &q, element *new_element);
void dequeue(queue &q, element *&result_element);
void traverse(queue q);
void hitungJumlah(queue q,int& jml);
void reverse(queue &q,element *elem,int jml);

int main(){
	element* p;
	int pilih,n;
	createQueue(q);
	do{
			cout << "==== Program Queue ==== \n";
			cout << " 1. enqueue \n";
			cout << " 2. dequeue \n";
			cout << " 3. traverse \n";
			cout << " 4. reverse \n";
			cout << " 5. exit \n";

			cout << " pilih menu : ";
			cin >> pilih;

					switch(pilih){
							case 1: 
							cout << " Program Enqueue \n";
							createElement(p);
							enqueue(q,p);
							break;

							case 2:
							cout << " Program dequeue \n";
							dequeue(q,p);
							break;

							case 3:
							traverse(q);
							break;

							case 4:
							reverse(q,p,n);
							traverse(q);
							break;

							case 5:
							cout << " exit program \n";
							break;

							default:
							cout << " no option \n";
							break;
					}
}while (pilih != 5);
cout << " program selesai \n";
	}


void createElement(element *&new_element){
		new_element = new element;
		cout << " data : ";
		cin >> new_element->info;
		new_element -> next = NULL;
}
void createElement2(element *&new_element, char& elm){
 new_element = new element;
 new_element -> info = elm;
 new_element -> next = NULL;
}

void enqueue(queue &q, element *new_element){
		if (q.front == NULL){

				q.front = new_element;
				q.back = new_element;
		}else{
				q.back->next = new_element;
				q.back = new_element;
		}
}

void dequeue(queue &q, element *&result_element){
		if (q.front == NULL){
				cout << " queue is empty \n";
		}else{
				result_element = q.front;
				q.front = q.front -> next;
				result_element->next = NULL;
		}
}

void traverse(queue q){
		
		if(q.front == NULL){
				cout << " queue is empty \n";
		}else{

				element *trav = q.front;
				cout << " antrian = [";
				while (trav != NULL){

						cout << trav->info;
						if(trav-> next != NULL){
								cout << ", ";
						}
						trav = trav ->next;
				}
				cout << "] " << endl;
		}

}

void hitungJumlah(queue q,int& jml){
			jml = 1;
			element *count = q.front;
			while(count->next != NULL){
				jml++;
				count = count-> next;
			}
}

void reverse(queue &q,element *elem,int jml){
		if(isEmpty(q)){
				cout << " ";
		}
		else{
				element *element2;
				createQueue(r);
				hitungJumlah(q,jml);

				for(int i = 0; i < jml; i++){
						for(int j=0; j < jml; j++){
							dequeue(q,elem);
							enqueue(q,elem);
						}

						createElement2(element2,q.front->info);
						enqueue(r,element2);
				}
				q = r;
				cout << " berhasil \n";
		}
}
